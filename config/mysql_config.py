mysql_config = {
    'host': 'localhost',
    'user': 'root',
    'password': 'xxx',
    'database': 'huokeyu',
    'port': 3306
}

# mysql数据类型，时间戳自动填充，自动更新
# TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

sql_dict = {
    'user_info': "CREATE TABLE user_info("
                 "id INT(32) NOT NULL AUTO_INCREMENT  COMMENT '主键' ,"
                 "account BIGINT(50) NOT NULL   COMMENT '账号' ,"
                 "password VARCHAR(255) NOT NULL   COMMENT '密码' ,"
                 "nikename VARCHAR(255) NOT NULL   COMMENT '用户昵称' ,"
                 "balance BIGINT NOT NULL  DEFAULT 0 COMMENT '账户余额;单位为分' ,"
                 "status TINYINT(1) NOT NULL  DEFAULT 0 COMMENT '状态;0启用，1禁用' ,"
                 "create_time DATETIME NOT NULL   COMMENT '创建时间' ,"
                 "update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' ,"
                 "PRIMARY KEY (id)"
                 ")  COMMENT = '用户信息';"
    ,
    'product_info': "CREATE TABLE product_info("
                    "id INT(32) NOT NULL AUTO_INCREMENT  COMMENT '主键' ,"
                    "pro_name VARCHAR(32) NOT NULL   COMMENT '商品名称' ,"
                    "inventory INT NOT NULL   COMMENT '商品库存' ,"
                    "price INT NOT NULL   COMMENT '价格;单位为分' ,"
                    "status TINYINT(1) NOT NULL  DEFAULT 0 COMMENT '状态;0上架，1下架，-1已删除' ,"
                    "create_user VARCHAR(255) NOT NULL   COMMENT '创建人' ,"
                    "create_time DATETIME NOT NULL   COMMENT '创建时间' ,"
                    "update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' ,"
                    "PRIMARY KEY (id)"
                    ")  COMMENT = '商品信息';"
    ,
    'order_from': "CREATE TABLE order_from("
                  "id INT(32) NOT NULL AUTO_INCREMENT  COMMENT '主键, 订单编号' ,"
                  "order_amount_total INT NOT NULL   COMMENT '订单金额;单位为分' ,"
                  "shop_id INT(32) NOT NULL   COMMENT '商品编号' ,"
                  "order_quantity INT(32) NOT NULL   COMMENT '购买数量' ,"
                  "status INT NOT NULL  DEFAULT 0 COMMENT '支付状态;0未支付，1已支付' ,"
                  "user_id INT NOT NULL   COMMENT '用户id' ,"
                  "shop_time TIMESTAMP NOT NULL   COMMENT '下单时间' ,"
                  "PRIMARY KEY (id)"
                  ")  COMMENT = '订单';"

}
