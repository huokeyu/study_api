from error_package.error import APIException


class CheckError(APIException):
    code = 400
    msg = "参数错误"
    error_code = 400


class DatabaseError(APIException):
    code = 501
    msg = '数据库操作失败'
    error_code = 501


class TokenError(APIException):
    code = 401
    msg = 'token过期,请重新登录'
    error_code = 401


class ParamsTypeError(APIException):
    code = 400
    msg = "参数类型错误"
    error_code = 400


class NoneError(APIException):
    code = 400
    msg = "参数为空"
    error_code = 400
