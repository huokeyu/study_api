from flask import request, json
from werkzeug.exceptions import HTTPException


# 重写异常状态码捕捉对象
class APIException(HTTPException):
    code = 500
    msg = "系统异常"
    error_code = 500

    def __init__(self, msg=None, code=None, error_code=None, headers='application/json'):
        self.headers = headers
        if code:
            self.code = code
        if error_code:
            self.error_code = error_code
        if msg:
            self.msg = msg
        super(APIException, self).__init__(msg, None)

    def get_body(self, environ=None, scope=None):
        body = dict(
            msg=str(self.msg),
            error_code=self.error_code,
            request=request.method + " " + self.get_url()
        )

        return json.dumps(body)

    def get_headers(self, environ=None, scope=None):
        return [("Content-Type", self.headers)]

    @staticmethod
    def get_url():
        full_path = str(request.full_path)
        main_path = full_path.split("?")
        return main_path[0]
