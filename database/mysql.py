from config.mysql_config import mysql_config as mc
import pymysql
from error_package.error_code import DatabaseError


class RunMysql:
    conn = None
    cur = None

    def __init__(self):
        try:
            self.conn = pymysql.connect(host=mc['host'], port=mc['port'], user=mc['user'], password=mc['password'],
                                        database=mc['database'], autocommit=True)  # 创建数据库连接对象,设置修改后自动提交
            self.cur = self.conn.cursor()
        except Exception as e:
            print(f'数据库连接失败:{e}')

    def __del__(self):
        # sql执行结束后，需要关闭游标与数据库连接，规则为先开后关，后开先关
        self.cur.close()
        self.conn.close()
        print('数据库连接已全部关闭')

    def run_sql(self, sql):
        try:
            self.cur.execute(sql)
            return self.cur.fetchall()
        except Exception as e:
            raise DatabaseError(e)

    def edit_sql(self, sql, value):
        try:
            self.cur.execute(sql, value)
        except Exception as e:
            raise DatabaseError(e)

    # 有条件查询
    def query_sql(self, sql, values, data_num=0):  # data_num:0返回全部数据，1返回单条数据
        try:
            self.cur.execute(sql, values)
            return self.cur.fetchall() if data_num == 0 else self.cur.fetchone()
        except Exception as e:
            raise DatabaseError(e)

    # 无条件查询
    def query_sql_no_param(self, sql, data_num=0):
        try:
            self.cur.execute(sql)
            return self.cur.fetchall() if data_num == 0 else self.cur.fetchone()
        except Exception as e:
            raise DatabaseError(e)
