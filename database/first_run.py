# 首次运行项目自动创建该项目所使用的数据库以及数据表
from config.mysql_config import mysql_config as mc
import pymysql
from database.mysql import RunMysql
from config.mysql_config import sql_dict


class FirstData:
    conn = None
    cur = None

    def __init__(self):
        try:
            self.conn = pymysql.connect(host=mc['host'], port=mc['port'], user=mc['user'], password=mc['password'],
                                        autocommit=True)  # 创建数据库连接对象,设置修改后自动提交
            self.cur = self.conn.cursor()
        except pymysql.Error as e:
            print(f'数据库连接失败:{e}')

    def __del__(self):
        # sql执行结束后，需要关闭游标与数据库连接，规则为先开后关，后开先关
        self.cur.close()
        self.conn.close()
        print('数据库连接已全部关闭')

    def add_sql(self, sql):
        try:
            self.cur.execute(sql)
        except pymysql.Error as e:
            print(f'添加失败:{e}')
        finally:
            self.cur.close()
            self.conn.close()

    def query_database(self):
        try:
            self.cur.execute("show databases")
            res = self.cur.fetchall()
            print(f'当前数据库:{res}')
            sql_list = []

            # 查询目前数据库内所有库并添加进sql_list
            for s in res:
                sql_list.append(s[0])

            if mc['database'] not in sql_list:
                self.add_sql(f'create database {mc["database"]}')
                print('数据库创建成功')
        except pymysql.Error as e:
            print(f'错误信息:{e}')

    @staticmethod
    def query_sheet():
        try:
            rs = RunMysql()
            res = rs.run_sql("show tables")
            print(f'当前数据表:{res}')
            table_list = []
            want_list = ['user_info', 'product_info', 'order_from']

            # 查询目前数据库内所有表并添加进table_list
            for s in res:
                table_list.append(s[0])

            # 现有表减去想添加的表，剩余的表就是需要创建的
            for del_str in table_list:
                want_list.remove(del_str)

            # 若存在缺失的数据表，则遍历列表并创建
            if want_list:
                for key in want_list:
                    rsql = sql_dict[key]
                    rs.run_sql(rsql)
                print('数据表创建成功')
        except pymysql.Error as e:
            print(f"添加失败:{e}")
