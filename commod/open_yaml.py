import os
import yaml

path = os.path.dirname(__file__).split("commod")[0]


class OpenYaml:
    def read_yaml(self, filename):
        with open(f"{path}/database/{filename}", mode='r') as f:
            return yaml.load(f.read(), Loader=yaml.FullLoader)

    def write_yaml(self, filename, json_data):
        try:
            with open(f"{path}/database/{filename}", mode="a", encoding="utf-8") as f:
                yaml.dump(json_data, stream=f, allow_unicode=True)
            return "write success"
        except Exception as e:
            return e


# op = OpenYaml()

