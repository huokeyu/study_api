from flask import make_response, jsonify


def currency(message='请求成功', data=None, code=200, statu='200'):
    res = make_response(jsonify({"message": message, 'mes_code': code, 'data': data}))
    res.status = statu
    return res
