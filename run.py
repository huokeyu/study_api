"""
    created by 霍科羽 on 2022/5/29.
"""
from werkzeug.exceptions import HTTPException
from error_package.error import APIException
from server.server import create_server
from database.first_run import FirstData

server = create_server()
fd = FirstData()
fd.query_database()
fd.query_sheet()


# 全局未知异常捕捉
@server.errorhandler(Exception)
def framework_error(e):
    if isinstance(e, APIException):
        return e
    if isinstance(e, HTTPException):
        code = e.code
        msg = e.description
        error_code = 10086
        return APIException(msg, code, error_code)
    else:
        return APIException()


if __name__ == '__main__':
    server.run(host='0.0.0.0', port='3090')
