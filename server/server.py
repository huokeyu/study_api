from flask import Flask
from api import user, product, order


def register_blueprints(server):
    server.register_blueprint(user.user)
    server.register_blueprint(product.product)
    server.register_blueprint(order.order)


def create_server():
    server = Flask(__name__)

    register_blueprints(server)
    return server
