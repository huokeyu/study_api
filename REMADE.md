

                                           简介  
1.目前实现功能：   
1.1.注册  
1.2.登录  
1.3.修改密码  
1.4.更改昵称  
1.5.充值  
1.6.获取商品列表  
1.7.新增商品  
1.8.编辑商品  
1.9.查询订单    
1.10.创建订单  
1.11.订单支付

2.本项目所使用软件  
2.1.MySql==8.0  
2.1.NaviCat==15

3.项目内所使用第三方库  
3.1.项目根路径下pipfile.txt  
3.2.pip install -r pipfile.txt 即可快速安装本项目所使用第三方库

4.项目配置  
4.1.config/mysql_config中mysql_config字典需要手动配置为本地MySql配置，才能正常启动项目  
4.2.MySql安装过程除密码外配置都是默认的话，4.1中的配置仅需修改password即可  
4.3.项目启动文件为根目录run.py文件

5.接口文档：  
https://console-docs.apipost.cn/preview/c678104a82384cf8/faf02ec0c99fc1f3

6.数据库文档：
![](数据库.png)