from flask import Blueprint, request
from commod.return_data import currency
from database.mysql import RunMysql
from validate.check_token import check_token
from validate.check.check_product import CheckProduct
from time import strftime

product = Blueprint('product', __name__, url_prefix="/product")

timer = str(strftime('%Y-%m-%d %H:%M:%S'))


@product.route('/list', methods=["POST"])
@check_token
def pro_list():
    data = request.json
    CheckProduct(data).check_query()
    rs = RunMysql()
    json_res = []
    if data:
        cond = get_data()
        result = rs.query_sql(cond[0], (
            f"%{cond[1]['proname']}%", cond[1]['startprice'] * 100, cond[1]['endprice'] * 100, cond[1]['startinventory'],
            cond[1]['endinventory']))
    else:
        result = rs.query_sql_no_param("select pro_name, inventory, price, status, create_user, id from product_info")

    # 构造返回参数
    for index in result:
        dict_res = dict()
        dict_res['pro_name'] = index[0]
        dict_res['inventory'] = index[1]
        dict_res['price'] = index[2] / 100
        dict_res['status'] = index[3]
        dict_res['create_user'] = index[4]
        dict_res['id'] = index[5]
        json_res.append(dict_res)
    return currency(data=json_res)


# 构造sql查询条件
def get_data():
    sql = "select pro_name, inventory, price, status, create_user, id from product_info where pro_name like %s and price " \
          "between %s and %s and inventory between %s and %s"

    res_dict = {'status': 'is not null', 'startprice': 0, 'endprice': 0x3f3f3f3f, 'startinventory': 0,
                'endinventory': 0x3f3f3f3f, 'proname': ''}
    if 'status' not in request.json:
        sql += ' and status is not null'
    else:
        sql += f' and status = {request.json["status"]}'
        del res_dict['status']
    for key, value in request.json.items():
        if key in res_dict:  # 若请求参数中含有sql条件，则将请求参数值覆盖默认值
            res_dict.update({key: value})

    return [sql, res_dict]


@product.route('/create', methods=["POST"])
@check_token
def pro_create():
    data = request.json
    CheckProduct(data).check_add()
    rs = RunMysql()
    name = rs.query_sql('select id from product_info where pro_name = %s', (data['product_name']))

    if not name:
        rs.edit_sql(
            'insert into product_info (pro_name,inventory,price,create_user,create_time) values(%s,%s,%s,%s,%s)',
            (data["product_name"], data["inventory"], data["price"] * 100, request.headers["user_id"], timer))
        return currency()
    else:
        return currency('商品名称重复')


@product.route('/update', methods=["POST"])
@check_token
def pro_update():
    data = request.json
    CheckProduct(data).check_update()
    rs = RunMysql()

    res_id = rs.query_sql("select id from product_info where id = %s", (data["pro_id"]))
    if res_id:
        sql_res = create_sql()

        sql_list = create_sql()[1]
        sql_list.append(data["pro_id"])

        rs.edit_sql(f"update product_info set {sql_res[0]} where id = %s", sql_list)
        return currency()
    else:
        return currency('未查询到商品信息')


# 构造sql更新语句
def create_sql():
    data = request.json.copy()
    del data['pro_id']
    _str = ""
    value_list = []
    for key, value in data.items():
        _str += f'{key}=%s,'
        if key == 'price':  # 若传参为价格，则value单位换算，元>>分
            value_list.append(value * 100)
        else:
            value_list.append(value)
    res_str = _str.rstrip(",")
    return [res_str, value_list]


@product.route('/delete', methods=["POST"])
@check_token
def pro_delete():
    data = request.json
    CheckProduct(data).check_delete()
    rs = RunMysql()

    result = rs.query_sql('select id from product_info where id = %s', (data['pro_id']))
    if result:
        rs.edit_sql('update product_info set status = %s where id = %s', (data['status'], data['pro_id']))
        return currency()
    else:
        return currency('未找到商品信息')
