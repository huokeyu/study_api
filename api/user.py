from flask import Blueprint, request
from commod.return_data import currency
from database.mysql import RunMysql
from validate.check_token import create_token, check_token
from validate.check.check_user import CheckUser
from time import strftime

user = Blueprint('user', __name__, url_prefix="/user")


@user.route('/register', methods=["POST"])
def register():
    data = request.json
    CheckUser(data).check_register()  # 参数校验
    rs = RunMysql()

    result = rs.query_sql('select id from user_info where account = %s', (data['account']))
    if not result:
        rs.edit_sql(
            'insert into user_info (account, password, nikename, create_time) values (%s,%s,%s,%s)',
            (data['account'], data['password'], data['nikename'], str(strftime('%Y-%m-%d %H:%M:%S'))))
        return currency()
    else:
        return currency('该账号已被注册')


@user.route('/login', methods=["POST"])
def login():
    data = request.json
    CheckUser(data).check_login()  # 参数校验
    rs = RunMysql()

    result = rs.query_sql('select id, password, status from user_info where account = %s', (data["account"]), 1)
    if result:
        if result[2] == 0:
            if result[1] == data['password']:
                return currency(data={"user_id": result[0], "user_token": create_token(data['account'])})
            else:
                return currency('密码错误')
        else:
            return currency('该账号已被禁用')
    else:
        return currency('该账号未注册')


@user.route('/edit/password', methods=["POST"])
@check_token
def edit_paswsword():
    data = request.json
    CheckUser(data).check_editpwd()  # 参数校验
    rs = RunMysql()

    result = rs.query_sql('select password from user_info where account = %s', (data["account"]), 1)
    if result:
        if result[0] == data['oldpassword']:
            rs.edit_sql('update user_info set password = %s where account = %s',
                        (data['newpassword'], data["account"]))
            return currency()
        else:
            return currency('原始密码错误')
    else:
        return currency('该账号未注册')


@user.route('/edit/nikename', methods=["POST"])
@check_token
def edit_nikename():
    data = request.json
    CheckUser(data).check_editname()  # 参数校验
    rs = RunMysql()

    result = rs.query_sql('select id from user_info where account = %s', (data["account"]), 1)
    if result:
        rs.edit_sql('update user_info set nikename = %s where account = %s',
                    (data['newnikename'], data["account"]))
        return currency()
    else:
        return currency('该账号未注册')


@user.route('/add/money', methods=["POST"])
@check_token
def add_money():
    data = request.json
    CheckUser(data).check_addmoney()  # 参数校验
    rs = RunMysql()

    result = rs.query_sql(f'select id, balance from user_info where account = %s', (data["account"]), 1)
    if result:
        rs.edit_sql(f'update user_info set balance = %s where account = %s',
                    (data['balance'] * 100 + result[1], data["account"]))
        return currency()
    else:
        return currency('该账号未注册')
