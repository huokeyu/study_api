from flask import Blueprint, request
from datetime import datetime
from database.mysql import RunMysql
from validate.check.check_order import CheckOrder
from validate.check_token import check_token
from commod.return_data import currency
import time

order = Blueprint('order', __name__, url_prefix="/order")


@order.route('/create', methods=["POST"])
@check_token
def order_create():
    data = request.json
    CheckOrder(data).check_order_create()
    rs = RunMysql()

    user = request.headers['user_id']
    product = rs.query_sql('select id, price, inventory, status from product_info where id = %s', (data['pro_id']), 1)

    if product:
        if product[3] != 0:  # 判断商品是否上架
            return currency('当前商品未上架')
        if data['amount_total'] == product[1] / 100:  # 核对商品价格与订单金额
            if data['quantity'] < product[2]:  # 核对商品库存是否充裕
                rs.edit_sql(
                    'insert into order_from (order_amount_total, shop_id, user_id, shop_time, order_quantity) values (%s,%s,%s,%s,%s)',
                    (
                        data['amount_total'] * 100, data['pro_id'], user, time.strftime("%Y-%m-%d %H:%M:%S"),
                        data['quantity']))

                # 购买后商品减去相应数量库存
                rs.edit_sql('update product_info set inventory = %s where id = %s',
                            (product[2] - data['quantity'], data['pro_id']))
                return currency()
            else:
                return currency('商品库存不足')
        else:
            return currency('订单金额错误')
    else:
        return currency('未查询到商品信息')


@order.route('/query', methods=["POST"])
@check_token
def order_query():
    data = request.json
    CheckOrder(data).check_order_query()
    rs = RunMysql()
    res_json = []

    if data:
        cond = order_get_sql()
        result = rs.query_sql(cond[0], (
            cond[1]['startorder_amount_total'] * 100, cond[1]['endorder_amount_total'] * 100, cond[1]['startshop_time'],
            cond[1]['endshop_time']))
    else:
        result = rs.query_sql_no_param(
            "select id, order_amount_total, shop_id, user_id, status, shop_time from order_from")

    # 构造返回参数
    for index in result:
        dict_res = dict()
        dict_res['order_id'] = index[0]
        dict_res['order_amount_total'] = index[1] / 100
        dict_res['shop_id'] = index[2]
        dict_res['user_id'] = index[3]
        dict_res['status'] = index[4]
        dict_res['shop_time'] = datetime.strftime(index[5], '%Y-%m-%d %H:%M:%S')
        res_json.append(dict_res)

    return currency(data=res_json)


# 构造sql
def order_get_sql():
    res_sql = "select id, order_amount_total, shop_id, user_id, status, shop_time from order_from where " \
              "order_amount_total between %s and %s and shop_time between %s and %s "

    sql_dict = {'startorder_amount_total': 0, 'endorder_amount_total': 0x3f3f3f3f,
                'startshop_time': '1970-01-01 00:00:00', 'endshop_time': '3000-01-01 00:00:00'}

    if 'status' in request.json:
        res_sql += f' and status = {request.json["status"]}'
    else:
        res_sql += ' and status is not null'

    if 'user_id' in request.json:
        res_sql += f' and user_id = {request.json["user_id"]}'
    else:
        res_sql += ' and user_id is not null'

    for key, value in request.json.items():
        if key in sql_dict:  # 若请求参数中含有sql条件，则将请求参数值覆盖默认值
            sql_dict.update({key: value})

    return [res_sql, sql_dict]


@order.route('/payment', methods=["POST"])
@check_token
def order_payment():
    data = request.json
    CheckOrder(data).check_order_payment()
    rs = RunMysql()

    user = rs.query_sql('select id, balance from user_info where id = %s', (request.headers['user_id']), 1)  # 获取用户信息

    # 获取订单信息
    order_info = rs.query_sql('select id, order_amount_total, user_id, status from order_from where id = %s',
                              (data['order_id']), 1)

    if order_info:
        if user[0] != order_info[2]:
            return currency('当前用户无此订单')
        if order_info[3] == 1:
            return currency('订单不可重复支付')
        if user[1] > order_info[1]:
            rs.edit_sql('update user_info set balance = %s where id = %s', (user[1] - order_info[1], user[0]))
            rs.edit_sql('update order_from set status = 1 where id = %s', (data['order_id']))
            return currency()
        else:
            return currency('账户余额不足，请及时充值')
    else:
        return currency('订单不存在')
