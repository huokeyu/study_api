from error_package.error_code import CheckError, NoneError
from validate.check_tool import check_parlen


class CheckUser:
    def __init__(self, data):
        self.data = data

    def check_register(self):
        try:
            if self.data["account"] and self.data["password"] and self.data["nikename"]:
                if isinstance(self.data["account"], int) and isinstance(self.data["password"], str) and isinstance(
                        self.data["nikename"], str):
                    if len(str(self.data["account"])) == 11 and len(self.data["password"]) >= 6 and len(
                            self.data["nikename"]) < 20:
                        check_parlen(self.data, 3)
                    else:
                        raise CheckError('参数不符要求')
                else:
                    raise CheckError('参数类型不符合要求')
        except KeyError:
            raise NoneError

    def check_login(self):
        try:
            if self.data["account"] and self.data["password"]:
                if isinstance(self.data["account"], int) and isinstance(self.data["password"], str):
                    if len(str(self.data["account"])) == 11 and len(self.data["password"]) >= 6:
                        check_parlen(self.data, 2)
                    else:
                        raise CheckError('参数不符要求')
                else:
                    raise CheckError('参数类型不符合要求')
        except KeyError:
            raise NoneError

    def check_editpwd(self):
        try:
            if self.data["account"] and self.data["oldpassword"] and self.data["newpassword"]:
                if isinstance(self.data["account"], int) and isinstance(self.data["oldpassword"], str) and isinstance(
                        self.data["newpassword"], str):
                    if len(str(self.data["account"])) == 11 and len(self.data["oldpassword"]) >= 6 and len(
                            self.data["newpassword"]) >= 6:
                        check_parlen(self.data, 3)
                    else:
                        raise CheckError('参数不符要求')
                else:
                    raise CheckError('参数类型不符合要求')
        except KeyError:
            raise NoneError

    def check_editname(self):
        try:
            if self.data["account"] and self.data["newnikename"]:
                if isinstance(self.data["account"], int) and isinstance(self.data["newnikename"], str):
                    if len(str(self.data["account"])) == 11 and len(self.data["newnikename"]) < 10:
                        check_parlen(self.data, 2)
                    else:
                        raise CheckError('参数不符要求')
                else:
                    raise CheckError('参数类型不符合要求')
        except KeyError:
            raise NoneError

    def check_addmoney(self):
        try:
            if self.data['account'] and self.data["balance"]:
                if isinstance(self.data['account'], int):
                    tp = type(self.data["balance"])
                    if tp == float and len(str(self.data["balance"]).split(".")[1]) < 3 or tp == int:
                        if len(str(self.data["account"])) == 11 and self.data["balance"] < 10001:
                            check_parlen(self.data, 2)
                        else:
                            raise CheckError('参数不符要求')
                    else:
                        raise CheckError('参数类型不符合要求')
                else:
                    raise CheckError('参数类型不符合要求')
        except KeyError:
            raise NoneError
