from error_package.error_code import CheckError, NoneError
from validate.check_tool import check_type, check_params, check_parlen, check_date


class CheckOrder:

    def __init__(self, data):
        self.data = data

    def check_order_query(self):
        params = ['user_id', 'status', 'startorder_amount_total', 'endorder_amount_total', 'startshop_time',
                  'endshop_time']
        if self.data:
            if not set(self.data.keys()).difference(params):  # 判断传入参数是否正确
                check_params(list(self.data.values()))
                par_list = list(self.data.keys())
                for par in par_list:
                    if par in ['startshop_time', 'endshop_time']:  # 日期时间类型单独处理
                        check_date(self.data[par])
                    elif par == 'status':
                        if self.data[par] in [0, 1]:
                            check_type(p1={self.data[par]: int})
                        else:
                            raise CheckError
                    else:
                        check_type(p1={self.data[par]: int})
                        check_parlen(self.data[par], 10000, 1)

            else:
                raise CheckError

    def check_order_create(self):
        try:
            check_params([self.data['pro_id'], self.data['amount_total'], self.data['quantity']])
            for key, value in self.data.items():
                if key == 'amount_total':
                    check_type(p1={value: [int, float]})
                else:
                    check_type(p1={value: int})
            check_parlen(self.data['amount_total'], 10000, 1)
            check_parlen(self.data['quantity'], 10000, 1)
        except KeyError:
            raise NoneError

    def check_order_payment(self):
        try:
            check_params([self.data['order_id']])
            check_type(p1={self.data['order_id']: int})
        except KeyError:
            raise NoneError
