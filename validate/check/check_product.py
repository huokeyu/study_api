from error_package.error_code import CheckError, NoneError
from validate.check_tool import check_type, check_params, check_parlen


class CheckProduct:
    def __init__(self, data):
        self.data = data

    def check_query(self):
        params = ["status", "startprice", "endprice", "startinventory", "endinventory", "proname"]
        if self.data:  # 是否传入参数
            if not set(self.data.keys()).difference(params):  # 传入参数是否正确
                check_params(list(self.data.values()))  # 校验参数是否有空值
                par_list = list(self.data.keys())
                for par in par_list:
                    if par == "proname":  # proname值为字符串，单独处理
                        check_type(p1={self.data[par]: str})
                        check_parlen(self.data[par], 11)
                    else:
                        if par == 'status':  # status值为枚举值，单独处理
                            if self.data[par] not in [0, 1, -1]:
                                raise CheckError
                        else:
                            check_type(p1={self.data[par]: int})
                            check_parlen(self.data[par], 10000, 1)
            else:
                raise CheckError

    def check_add(self):
        try:
            check_params([self.data["product_name"], self.data["inventory"], self.data["price"]])
            check_type(p1={self.data["product_name"]: str}, p2={self.data["inventory"]: int},
                       p3={self.data["price"]: [int, float]})

            check_parlen(self.data["price"], 10000, 1)
            check_parlen(self.data["inventory"], 10000, 1)
            check_parlen(self.data["product_name"], 10)
            check_parlen(self.data, 3)
        except KeyError:
            raise NoneError

    def check_update(self):
        params = ['pro_id', 'pro_name', 'inventory', 'price']
        try:
            self.data['pro_id'] and self.data['pro_name']
        except KeyError:
            raise NoneError
        if not set(self.data.keys()).difference(params):
            check_params(list(self.data.values()))
            par_list = list(self.data.keys())

            for par in par_list:
                if par == 'pro_name':
                    check_type(p1={self.data[par]: str})
                    check_parlen(self.data[par], 11)
                else:
                    check_type(p1={self.data[par]: int})
                    check_parlen(self.data[par], 10000, 1)
        else:
            raise CheckError

    def check_delete(self):
        try:
            check_params([self.data['pro_id'], self.data['status']])
            check_type(p1={self.data['pro_id']: int}, p2={self.data['status']: int})
            if self.data['status'] not in [0, 1, -1]:
                raise CheckError
        except KeyError:
            raise NoneError
