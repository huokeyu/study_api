from error_package.error_code import ParamsTypeError, CheckError, NoneError
import time


def check_params(*args: list):
    for x in args[0]:
        if not str(x):
            raise NoneError


def check_type(**kwargs):
    for value in kwargs.items():
        for key, values in value[1].items():
            if type(values) is not list:
                if type(key) is not values:
                    raise ParamsTypeError
            else:
                if type(key) not in values:
                    raise ParamsTypeError
                else:
                    if type(key) is float and len(str(key).split(".")[1]) > 2:  # 浮点数小数位不可大于2位
                        raise ParamsTypeError


def check_parlen(data, lgth: int, ctype=0):  # ctype: 0比较长度，1比较大小
    if ctype == 0:
        if len(data) > lgth:
            raise CheckError()
    else:
        if data > lgth:
            raise CheckError()


def check_date(date_text):
    try:
        time.strptime(date_text, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        raise CheckError
