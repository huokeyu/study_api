import base64
import random
import time
from flask import request
from database.mysql import RunMysql
from functools import wraps
from error_package.error_code import TokenError


def create_token(acc: int):
    token = base64.b64encode((":".join([str(acc), str(random.random()), str(time.time() + 86400)])).encode()).decode()
    return token


# 鉴权装饰器
def check_token(fun):
    @wraps(fun)
    def wrap():
        try:
            params = request.headers  # 获取请求头
            token = base64.b64decode(params['user_token'])  # 解析token
            _token = token.decode().split(":")
            if time.time() < float(_token[2]):  # 判断token是否过期
                rs = RunMysql()
                res_user = rs.query_sql(f'select account from user_info where id = %s', (params["user_id"]), 1)  # 获取用户账号
                if res_user and int(_token[0]) == res_user[0]:
                    res = fun()
                else:
                    raise TokenError('请检查user_id与user_token')
            else:
                raise TokenError()
        except Exception:
            raise
        return res

    return wrap
